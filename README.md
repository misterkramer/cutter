[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg?style=flat-square)](https://conventionalcommits.org)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

# Cutter. Simple URL shortener

Yet another bitly :smile:

Docs [here](docs/cutter.md)

Main stack:

```text
python 3.9
docker
hashids
plotly dash with bootstrap components
boto3
```

For custom 404 page thanks [this codepen](https://codepen.io/UnfocusedDrive/pen/npDyq).
