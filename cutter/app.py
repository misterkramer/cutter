import logging
from datetime import datetime
from urllib.parse import urlparse

import boto3
import dash
import dash_bootstrap_components as dbc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from flask import abort, redirect, render_template
from hashids import Hashids

from cutter import app_settings
from cutter.__version__ import __version__

logging.basicConfig(
    format="[%(asctime)s] [%(levelname)s] [%(threadName)s] [%(funcName)s] (%(lineno)d:)%(message)s",
    level=logging.INFO,
)
app = dash.Dash(__name__, title="Cutter")
server = app.server
ddb = boto3.resource("dynamodb", region_name=app_settings.region_name).Table(
    app_settings.dynamodb_table
)
ssm = boto3.client("ssm", region_name=app_settings.region_name)
hashids_salt = ""
try:
    salt = ssm.get_parameter(Name="/cutter/hashids_salt", WithDecryption=True)
    hashids_salt = salt["Parameter"]["Value"]
except Exception as err:
    logging.error(err)
hashids = Hashids(min_length=8, salt=hashids_salt)

navbar = dbc.NavbarSimple(
    brand="Cutter. Simple URL shortener",
    brand_href="https://gitlab.com/misterkramer/cutter",
    sticky="top",
    className="mb-5",
    children=html.H5(children=dbc.Badge(f"version {__version__}")),
    color="secondary",
)
input_group = dbc.InputGroup(
    [
        dbc.Input(type="url", placeholder="Enter the link here", id="input-group-input"),
        dbc.InputGroupAddon(
            dbc.Button("Submit", id="input-group-button", color="primary"),
        ),
    ]
)
app.layout = html.Div(
    children=[
        html.H1(children=navbar),
        dbc.Container(input_group),
        dbc.Container(html.Span(id="short-url-output", style={"vertical-align": "middle"})),
        dbc.Container(html.Span(id="err", style={"vertical-align": "middle", "color": "red"})),
    ]
)


@app.callback(
    [Output("short-url-output", "children"), Output("err", "children")],
    [Input("input-group-button", "n_clicks"), State("input-group-input", "value")],
)
def on_button_click(n_clicks, long_url):
    logging.info("Start")
    # If url has no scheme added default "http" scheme, e.g. google.com -> http://google.com.
    final_url = urlparse(long_url) if urlparse(long_url).scheme else urlparse(f"http://{long_url}")
    # simple checker, ignores case checking if url contains ip. probably could be better (without using regex)¯\_(ツ)_/¯
    is_valid_url = (
        all([final_url.scheme, final_url.netloc]) and len(final_url.netloc.split(".")) > 1
    )
    if n_clicks is None:
        return dash.no_update, dash.no_update
    elif is_valid_url:
        logging.info(f"Get long_url: {long_url}")
        # before process trim final url
        stp_url = final_url.geturl().strip()
        try:
            up_response = ddb.update_item(
                Key={"id": "__id"},
                ExpressionAttributeNames={"#counter": "counter"},
                ExpressionAttributeValues={":n": 1},
                UpdateExpression="ADD #counter :n",
                ReturnValues="UPDATED_NEW",
            )
            atomic_id = int(up_response["Attributes"]["counter"])
            logging.debug(up_response)
            hashid = hashids.encode(atomic_id)
            put_response = ddb.put_item(
                Item={
                    "id": hashid,
                    "long_url": str(stp_url),
                    "created_at": str(datetime.utcnow()),
                }
            )
            logging.info(f"Put: {put_response}")
            return f"Short link is {app_settings.domain_url}/{hashid}", ""
        except Exception as err:
            logging.error(err)
            return "", "An error occurred. Please try again later."
    else:
        return "", f"{long_url} is not a valid URL."


@server.route("/<id>")
def redirect_to_long_url(id):
    logging.info("Start")
    try:
        item = ddb.get_item(Key={"id": id})
    except Exception as err:
        logging.error(err)
        return abort(500)
    logging.info(f"Get response from dynamodb: {item}")
    if "Item" in item:
        long_url = item["Item"]["long_url"]
        logging.info(f"Redirect to {long_url}")
        return redirect(str(long_url), code=301)
    else:
        return render_template("404.html"), 404
