import uvicorn

from cutter.settings import ServerSettings

server_settings = ServerSettings()

if __name__ == "__main__":
    uvicorn.run(
        "cutter.app:server",
        host=server_settings.server_host,
        port=server_settings.server_port,
        interface="wsgi",
        lifespan="off",
        log_config=None,
    )
