from pydantic import BaseSettings


class ServerSettings(BaseSettings):
    server_host: str = "0.0.0.0"
    server_port: int = 8080


class AppSettings(BaseSettings):
    domain_url: str = "https://cutt.click"
    region_name: str = "us-west-2"
    dynamodb_table: str = "cutter"
