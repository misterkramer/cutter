FROM python:3.9.4-slim
LABEL maintainer="misterkramers@gmail.com"
WORKDIR /usr/local/app
COPY . .
RUN pip install --no-cache-dir  --upgrade pip \
    && pip install --no-cache-dir poetry==1.1.5 \
    && poetry config virtualenvs.create false \
    && poetry install --no-dev --no-interaction --no-ansi \
    && chown nobody -R /usr/local/app
ENV LANG=en_US.UTF-8 TZ=UTC
USER nobody
ENTRYPOINT ["python3", "-m", "cutter"]