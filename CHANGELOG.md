# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Important!
Changelog is updated *automatically* using [commitizen](https://github.com/commitizen-tools/commitizen). Manual edits are not required.
## 0.8.1 (2021-04-10)

### Fix

- remove  to update correctly ui

## 0.8.0 (2021-04-10)

### Feat

- up url validator

## 0.7.0 (2021-04-09)

### Feat

- **dockerfile**: up python to 3.9.4
- add input url validator

## 0.6.0 (2021-04-08)

### Feat

- add fun 404 template

## 0.5.0 (2021-04-08)

### Feat

- add custom 404 page

## 0.4.0 (2021-04-07)

### Feat

- up bootstrap theme

## 0.3.0 (2021-04-06)

### Feat

- **pyproject**: add license

## 0.2.1 (2021-04-06)

### Fix

- **pyproject**: up description

## 0.2.0 (2021-04-06)

### Feat

- get hashids_salt from ssm

## 0.1.1 (2021-04-06)

### Fix

- remove extra f-string

## 0.1.0 (2021-04-05)

### Fix

- remove aws creds and use IAM policy

### Feat

- **cicd**: up cicd process. Ref #1
- add custom favicon
- **cicd**: up review stage with staging env
- add exception handling
- add cicd. Ref #1
- add base cutter logic
